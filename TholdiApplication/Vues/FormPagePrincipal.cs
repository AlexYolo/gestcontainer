﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using TholdiApplication.Modeles;
using TholdiApplication.Vues;


namespace TholdiApplication.Vues
{
    public partial class FormPagePrincipal : Form
    {
        public FormPagePrincipal()
        {
            InitializeComponent();
        }

        private void FormPagePrincipal_Load(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new FormPageConnexion ()) );
            t.Start();
            this.Close();
        }

        private void inspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
 