﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TholdiApplication.Modeles;
using TholdiApplication.Vues;



namespace TholdiApplication
{
    public partial class FormPageConnexion : Form
    {
        public FormPageConnexion()
        {
            InitializeComponent();
        }

        private void textBoxNomUtilisateur_TextChanged(object sender, EventArgs e)
        {

        }

        private void boutonConnexion_Click(object sender, EventArgs e)
        {
            if (textBoxNomUtilisateur.Text == "docker")
            {
                Thread t = new Thread(() => Application.Run(new FormPagePrincipalDocker()));
                t.Start();
                this.Close();
                //MessageBox.Show("L'activité doit être composée d'au moins 3 caractères");
                //textBoxNomUtilisateur.Text = "";
                //textBoxNomUtilisateur.Focus();
            }
            if (textBoxNomUtilisateur.Text == "chefEquipe")
            {
                Thread t = new Thread(() => Application.Run(new FormPagePrincipalChefEquipe()));
                t.Start();
                this.Close();
            }
        }
    }
}
