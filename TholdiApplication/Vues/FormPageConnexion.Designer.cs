﻿namespace TholdiApplication
{
    partial class FormPageConnexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPageConnexion));
            this.boutonConnexion = new System.Windows.Forms.Button();
            this.textBoxNomUtilisateur = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxMotDePasse = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.logoTholdi = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.logoTholdi)).BeginInit();
            this.SuspendLayout();
            // 
            // boutonConnexion
            // 
            this.boutonConnexion.BackColor = System.Drawing.Color.Teal;
            this.boutonConnexion.Font = new System.Drawing.Font("Tahoma", 8F);
            this.boutonConnexion.ForeColor = System.Drawing.Color.White;
            this.boutonConnexion.Location = new System.Drawing.Point(193, 261);
            this.boutonConnexion.Name = "boutonConnexion";
            this.boutonConnexion.Size = new System.Drawing.Size(213, 40);
            this.boutonConnexion.TabIndex = 0;
            this.boutonConnexion.Text = "Connexion";
            this.boutonConnexion.UseVisualStyleBackColor = false;
            this.boutonConnexion.Click += new System.EventHandler(this.boutonConnexion_Click);
            // 
            // textBoxNomUtilisateur
            // 
            this.textBoxNomUtilisateur.Location = new System.Drawing.Point(193, 168);
            this.textBoxNomUtilisateur.Name = "textBoxNomUtilisateur";
            this.textBoxNomUtilisateur.Size = new System.Drawing.Size(212, 20);
            this.textBoxNomUtilisateur.TabIndex = 1;
            this.textBoxNomUtilisateur.Text = "test";
            this.textBoxNomUtilisateur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNomUtilisateur.TextChanged += new System.EventHandler(this.textBoxNomUtilisateur_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F);
            this.label1.Location = new System.Drawing.Point(250, 152);
            this.label1.MaximumSize = new System.Drawing.Size(120, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Identifiant";
            // 
            // textBoxMotDePasse
            // 
            this.textBoxMotDePasse.Location = new System.Drawing.Point(194, 217);
            this.textBoxMotDePasse.Name = "textBoxMotDePasse";
            this.textBoxMotDePasse.Size = new System.Drawing.Size(212, 20);
            this.textBoxMotDePasse.TabIndex = 3;
            this.textBoxMotDePasse.Text = "Entrez votre mot de passe";
            this.textBoxMotDePasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.label2.Location = new System.Drawing.Point(236, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mot de passe";
            // 
            // logoTholdi
            // 
            this.logoTholdi.BackColor = System.Drawing.Color.Transparent;
            this.logoTholdi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.logoTholdi.Image = global::TholdiApplication.Properties.Resources.logo1;
            this.logoTholdi.Location = new System.Drawing.Point(195, 39);
            this.logoTholdi.Name = "logoTholdi";
            this.logoTholdi.Size = new System.Drawing.Size(211, 99);
            this.logoTholdi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoTholdi.TabIndex = 5;
            this.logoTholdi.TabStop = false;
            // 
            // FormPageConnexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TholdiApplication.Properties.Resources.IHMBGRD;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(640, 328);
            this.Controls.Add(this.logoTholdi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxMotDePasse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxNomUtilisateur);
            this.Controls.Add(this.boutonConnexion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormPageConnexion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tholdi - Connexion";
            ((System.ComponentModel.ISupportInitialize)(this.logoTholdi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button boutonConnexion;
        private System.Windows.Forms.TextBox textBoxNomUtilisateur;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxMotDePasse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox logoTholdi;
    }
}