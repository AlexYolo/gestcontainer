﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using TholdiApplication.Modeles;
using TholdiApplication.Vues;


namespace TholdiApplication.Vues
{
    public partial class FormPagePrincipalChefEquipe : Form
    {
        public FormPagePrincipalChefEquipe()
        {
            InitializeComponent();
        }

        //private void prévoirUneInspectionToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    Thread t = new Thread(() => Application.Run(new FormPrevoirInspectionChef()));
        //    t.Start();
        //   this.Close();
        //}

        private void saisirUneInspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new FormSaisirInspectionChef()));
            t.Start();
            this.Close();
        }

        private void consulterUneInspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new FormConsulterInspectionChef()));
            t.Start();
            this.Close();
        }

        private void consulterLesProblèmesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new FormConsulterProblemeChef()));
            t.Start();
            this.Close();
        }

        private void RetournerVersLeMenuPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new FormPagePrincipal()));
            t.Start();
            this.Close();
        }

        private void prévoirUneInspectionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new FormPrevoirInspectionChef()));
            t.Start();
            this.Close();
        }
    }
}
