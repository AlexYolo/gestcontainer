﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiApplication.Modeles
{
    class Declaration
    {
        private int numDeclaration;
        private string commentaire;
        private DateTime dateDeclaration;
        private string libelleProbleme;
        private bool urgence;
        private bool traite;
        private Probleme unProbleme;
        private Containers unContainer;
        #region Propriété
        public int NumDeclaration
        {
            get
            {
                return numDeclaration;
            }
        }

        public string Commentaire
        {
            get
            {
                return commentaire;
            }

            set
            {
                commentaire = value;
            }
        }

        public DateTime DateDeclaration
        {
            get
            {
                return dateDeclaration;
            }

            set
            {
                dateDeclaration = value;
            }
        }

        public bool Urgence
        {
            get
            {
                return urgence;
            }

            set
            {
                urgence = value;
            }
        }

        public bool Traite
        {
            get
            {
                return traite;
            }

            set
            {
                traite = value;
            }
        }

        public Probleme Probleme
        {
            get
            {
                return unProbleme;
            }

            set
            {
                unProbleme = value;
            }
        }

        public Containers Container
        {
            get
            {
                return unContainer;
            }

            set
            {
                unContainer = value;
            }
        }

        public string LibelleProbleme
        {
            get
            {
                return libelleProbleme;
            }

            set
            {
                libelleProbleme = value;
            }
        }
        #endregion
        //public Declaration(string commentaire, Probleme probleme, bool urgence, bool traite, Containers container)
        //{
        //    this.Commentaire = commentaire;
        //    this.DateDeclaration = DateTime.Now;
        //    this.Urgence = urgence;
        //    this.Traite = traite;
        //    this.Probleme = probleme;
        //    this.Container = container;
        //}

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
            "SELECT CODEDECLARATION, COMMENTAIRE, DATEDECLARATION, URGENCE, TRAITE, NUMCONTAINER FROM DECLARATION";

        private static string _selectByIdSql =
            "SELECT CODEDECLARATION, COMMENTAIRE, DATEDECLARATION, URGENCE, TRAITE, CODEPROBLEME, NUMCONTAINER FROM DECLARATION WHERE CODEDECLARATION = ?CodeDeclaration ";

        private static string _updateSql =
            "UPDATE DECLARATION SET COMMENTAIRE = ?Commentaire, DATEDECLARATION=?DateDeclaration , URGENCE=?Urgence, TRAITE=?Traite, CODEPROBLEME=?CodeProbleme, NUMCONTAINER=?NumContainer  WHERE CODEDECLARATION=?CodeDeclaration ";

        private static string _insertSql =
            "INSERT INTO DECLARATION (COMMENTAIRE, DATEDECLARATION, URGENCE, TRAITE, CODEPROBLEME, NUMCOMMENTAIRE) VALUES (?Commentaire,?DateDeclaration,?Urgence,?CodeProbleme, ?NumCommentaire)";

        private static string _deleteByIdSql =
            "DELETE FROM DECLARATION WHERE CODEDECLARATION = ?CodeDeclaration";

        private static string _getLastInsertId =
            "SELECT CODEDECLARATION FROM DECLARATION WHERE COMMENTAIRE = ?Commentaire AND DATEDECLARATION=?DateDeclaration AND URGENCE=?Urgence AND TRAITE=?Traite AND CODEPROBLEME=?CodeProbleme AND NUMCONTAINER=?NumContainer";

   

        #endregion

        #region Méthodes de lien avec la base de donnée

        /// <summary>
        /// Valorise un objet contact depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="numDeclaration">La valeur de la clé primaire</param>
        public static Declaration Fetch(int numDeclaration)
        {
            Declaration d = null;
            MySqlConnection msc = DataBaseAccess.Connexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?CODEDECLARATION", numDeclaration));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                d = new Declaration();//Initialisation de la variable Client
                d.numDeclaration = Convert.ToInt16(jeuEnregistrements["CODEDECLARATION"].ToString());//Lecture d'un champ de l'enregistrement
                d.LibelleProbleme = jeuEnregistrements["LIBELLEPROBLEME"].ToString();
                d.commentaire = jeuEnregistrements["COMMENTAIRE"].ToString();
                d.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["DATEDECLARATION"].ToString());
                d.commentaire = jeuEnregistrements["COMMENTAIRE"].ToString();
                d.urgence = Convert.ToBoolean(jeuEnregistrements["URGENCE"].ToString());
                d.traite = Convert.ToBoolean(jeuEnregistrements["TRAITE"].ToString());
                string numContain = jeuEnregistrements["NUMCONTAINER"].ToString();
                d.unContainer = Containers.Fetch(Convert.ToInt32(numContain));


            }
            msc.Close();//fermeture de la connexion
            return d;
        }

        /// <summary>
        /// Retourne une collection contenant les contacts
        /// </summary>
        /// <returns>Une collection de contacts</returns>
        public static List<Declaration> FetchAll()
        {
            List<Declaration> resultat = new List<Declaration>();
            MySqlConnection msc = DataBaseAccess.Connexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration d = new Declaration();
                d = new Declaration();//Initialisation de la variable Client
                d.numDeclaration = Convert.ToInt16(jeuEnregistrements["CODEDECLARATION"].ToString());//Lecture d'un champ de l'enregistrement
                d.LibelleProbleme = jeuEnregistrements["LIBELLEPROBLEME"].ToString();
                d.commentaire = jeuEnregistrements["COMMENTAIRE"].ToString();
                d.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["DATEDECLARATION"].ToString());
                d.commentaire = jeuEnregistrements["COMMENTAIRE"].ToString();
                d.urgence = Convert.ToBoolean(jeuEnregistrements["URGENCE"].ToString());
                d.traite = Convert.ToBoolean(jeuEnregistrements["TRAITE"].ToString());
                string numContain = jeuEnregistrements["NUMCONTAINER"].ToString();
                d.unContainer = Containers.Fetch(Convert.ToInt32(numContain));
                resultat.Add(d);

            }
            msc.Close();
            return resultat;
        }
        #endregion

    }
}