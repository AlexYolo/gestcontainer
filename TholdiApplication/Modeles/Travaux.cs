﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TholdiApplication.Modeles
{
    class Travaux
    {
        public string codeTravaux { get; private set; }
        public string libelleTravaux { get; set; }
        public int dureeImmobilisation { get; set; }

        public Travaux(string code, string libelle, int duree)
        {
            codeTravaux = code;
            libelleTravaux = libelle;
            dureeImmobilisation = duree;
        }
        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
            "SELECT CODETRAVAUX , LIBELLETRAVAUX, DUREEIMMOBILISATION FROM TRAVAUX";

        private static string _selectByCodeTravauxSql =
            "SELECT CODETRAVAUX , LIBELLETRAVAUX, DUREEIMMOBILISATION FROM TRAVAUX WHERE CODETRAVAUX = ?CODETRAVAUX ";

        private static string _updateSql =
            "UPDATE TRAVAUX SET LIBELLETRAVAUX = ?LIBELLETRAVAUX, DUREEIMMOBILISATION=?DUREEIMMOBILISATION , TELEPHONE=?TELEPHONE, EMAIL=?EMAIL  WHERE CODETRAVAUX=?CODETRAVAUX ";

        private static string _insertSql =
            "INSERT INTO TRAVAUX (CODETRAVAUX,LIBELLETRAVAUX, DUREEIMMOBILISATION) VALUES (?CODEIMMOBILISATION,?LIBELLETRAVAUX,?DUREEIMMOBILISATION)";

        private static string _deleteByCodeTravauxSql =
            "DELETE FROM TRAVAUX WHERE CODETRAVAUX = ?CODETRAVAUX";

        private static string _getLastInsertCodeTravaux =
            "SELECT CODETRAVAUX FROM TRAVAUX WHERE LIBELLETRAVAUX = ?LIBELLETRAVAUX AND DUREEIMMOBILISATION=?DUREEIMMOBILISATION";
        #endregion
    }
}
