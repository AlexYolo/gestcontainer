﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiApplication.Modeles
{
    class Probleme
    {
        private short codeProbleme;
        private string libelleProbleme;
        public Probleme()
        {
            codeProbleme = -1;
        }

        public short CodeProbleme
        {
            get
            {
                return codeProbleme;
            }
        }

        public string LibelleProbleme
        {
            get
            {
                return libelleProbleme;
            }

            set
            {
                libelleProbleme = value;
            }
        }
        public override string ToString()
        {
            return String.Format("LIBELLEPROBLEME : {0}  ", libelleProbleme);
        }

        //public Probleme(string libelleProbleme)
        //{
        //    this.LibelleProbleme = libelleProbleme;

        //}
        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectProblemeSql =
            "SELECT CODEPROBLEME, LIBELLEPROBLEME FROM PROBLEME";

        private static string _selectByCodeProblemeSql =
            "SELECT CODEPROBLEME , LIBELLEPROBLEME FROM PROBLEME WHERE CODEPROBLEME = ?CODEPROBLEME ";

        private static string _updateSql =
            "UPDATE PROBLEME SET LIBELLEPROBLEME = ?LIBELLEPROBLEME WHERE CODEPROBLEME=?CODEPROBLEME ";

        private static string _insertSql =
            "INSERT INTO PROBLEME (CODEPROBLEME, LIBELLEPROBLEME) VALUES (?CODEPROBLEME,?LIBELLEPROBLEME)";

        private static string _deleteByCodeProblemeSql =
            "DELETE FROM PROBLEME WHERE CODEPROBLEME = ?CODEPROBLEME";

        private static string _getLastInsertCodeProbleme =
            "SELECT CODEPROBLEME FROM PROBLEME WHERE LIBELLEPROBLEME = ?LIBELLEPROBLEME";
        #endregion


        #region Méthodes de lien avec la base de donnée

        /// <summary>
        /// Valorise un objet contact depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="codeProbleme">La valeur de la clé primaire</param>
        public static Probleme Fetch(int codeProbleme)
        {
            Probleme p = null;
            MySqlConnection msc = DataBaseAccess.Connexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByCodeProblemeSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?CODEPROBLEME", codeProbleme));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                p = new Probleme();//Initialisation de la variable Client
                p.codeProbleme = Convert.ToInt16(jeuEnregistrements["CODEPROBLEME"].ToString());//Lecture d'un champ de l'enregistrement
                p.libelleProbleme = jeuEnregistrements["LIBELLEPROBLEME"].ToString();
            }
            msc.Close();//fermeture de la connexion
            return p;
        }

        /// <summary>
        /// Retourne une collection contenant les contacts
        /// </summary>
        /// <returns>Une collection de contacts</returns>
        public static List<Probleme> FetchAll()
        {
            List<Probleme> resultat = new List<Probleme>();
            MySqlConnection msc = DataBaseAccess.Connexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectProblemeSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Probleme p = new Probleme();
                string codeProblemo = jeuEnregistrements["CODEPROBLEME"].ToString();
                p.codeProbleme = Convert.ToInt16(codeProblemo);
                p.libelleProbleme = jeuEnregistrements["LIBELLEPROBLEME"].ToString();
                resultat.Add(p);
            }
            msc.Close();
            return resultat;
        }
        #endregion

    }
}