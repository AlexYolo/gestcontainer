﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace TholdiApplication.Modeles
{
    [Serializable]
    /// <summary>
    /// Classe Inspection : Représente une inspection
    /// </summary>
    class Inspection
    {
        private int _numContainer;
        private int _numInspection;
        private int _codeMotif;
        private int _codeEtat;
        private DateTime _dateInspection;
        private string _commentairePostInspection;
        private string _avis;
        private List<Containers> _lesContainers;
        private List<Motif> _lesMotifs;
        private List<Etat> _lesEtats;

        /// <summary>
        /// Obtient la collection de containers
        /// </summary>
        /// <remarks>La collection est en lecture seule</remarks>
        public ReadOnlyCollection<Containers> Container
        {
            get
            {
                return new ReadOnlyCollection<Containers>(_lesContainers);
            }

        }
        /// <summary>
        /// Obtient la collection de motifs
        /// </summary>
        /// <remarks>La collection est en lecture seule</remarks>
        public ReadOnlyCollection<Motif> Motif
        {
            get
            {
                return new ReadOnlyCollection<Motif>(_lesMotifs);
            }

        }
        /// <summary>
        /// Obtient la collection d'états
        /// </summary>
        /// <remarks>La collection est en lecture seule</remarks>
        public ReadOnlyCollection<Etat> Etat
        {
            get
            {
                return new ReadOnlyCollection<Etat>(_lesEtats);
            }
        }

        //}
        /// <summary>
        /// Obtient le numéro du container
        /// </summary>
        public int NumeroContainer
        {
            get { return _numContainer; }
        }
        /// <summary>
        /// Obtient le numéro de l'inspection 
        /// </summary>
        public int NumeroInspection
        {
            get { return _numInspection; }
        }
        /// <summary>
        /// Obtient le code du motif
        /// </summary>
        public int CodeMotif
        {
            get { return _codeMotif; }
        }
        /// <summary>
        /// Obtient le code d'état
        /// </summary>
        public int CodeEtat
        {
            get { return _codeEtat; }
        }
        /// <summary>
        /// Obtient ou définit la date de l'inspection
        /// </summary>
        public DateTime DateInspection
        {
            get { return _dateInspection; }
            set { _dateInspection = value; }
        }
        /// <summary>
        /// Obtient ou définit le commentaire 
        /// </summary>
        public string Commentaire
        {
            get { return _commentairePostInspection; }
            set { _commentairePostInspection = value; }
        }
        /// <summary>
        /// Obtient ou définit l'avis
        /// </summary>
        public string Avis
        {
            get { return _avis; }
            set { _commentairePostInspection = value; }
        }

        /// <summary>
        /// Initialise une inspection
        /// </summary>
        /// <param name="numContainer">Le numéro du container</param>
        /// <param name="numInspection">Le numéro de l'inspection</param>
        /// <param name="codeMotif">Le code du motif</param>
        /// <param name="codeEtat">Le code de l'état</param>
        /// <param name="dateInspection">La date de l'inspection</param>
        /// <param name="commentairePostInspection">Le commentaire post inspection</param>
        /// <param name="avis">L'avis de l'inspection</param>
        public Inspection(int numContainer, int numInspection, int codeMotif, int codeEtat, DateTime dateInspection, string commentaire, string avis)
        {
            _numContainer = numContainer;
            _numInspection = numInspection;
            _codeMotif = codeMotif;
            _codeEtat = codeEtat;
            _dateInspection = dateInspection;
            _commentairePostInspection = commentaire;
            _avis = avis;
        }
        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
            "SELECT NUMCONTAINER, NUMINSPECTION, LIBELLEMOTIF, LIBELLEETAT, DATEINSPECTION, COMMENTAIREPOSTINSPECTION, AVIS FROM INSPECTION";

        private static string _selectByNumContainerSql =
            "SELECT NUMCONTAINER, NUMINSPECTION, LIBELLEMOTIF, LIBELLEETAT, DATEINSPECTION, COMMENTAIREPOSTINSPECTION, AVIS FROM INSPECTION WHERE NUMCONTAINER = ?NUMCONTAINER";

        private static string _updateSql =
            "UPDATE INSPECTION SET LIBELLEMOTIF = ?LIBELLEMOTIF, LIBELLEETAT = ?LIBELLEETAT, DATEINSPECTION = ?DATEINSPECTION, COMMENTAIREPOSTINSPECTION = ?COMMENTAIREPOSTINSPECTION, AVIS = ?AVIS WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO INSPECTION (NUMCONTAINER, NUMINSPECTION, LIBELLEMOTIF, LIBELLEETAT, DATEINSPECTION) VALUES (?NUMCONTAINER, ?NUMINSPECTION, ?LIBELLEMOTIF, ?LIBELLEETAT, ?DATEINSPECTION)";

        private static string _deleteByNumInspectionSql =
            "DELETE FROM INSPECTION WHERE NUMINSPECTION = ?NUMINSPECTION AND NUMCONTAINER = ?NUMCONTAINER";

        private static string _getLastInsertNumInspection =
            "SELECT NUMINSPECTION FROM INSPECTION WHERE NUMCONTAINER=?NUMCONTAINER AND LIBELLEMOTIF=?LIBELLEMOTIF AND LIBELLEETAT=?LIBELLEETAT AND DATEINSPECTION=?DATEINSPECTION AND COMMENTAIREPOSTINSPECTION=?COMMENTAIREPOSTINSPECTION AND AVIS=?AVIS";
        #endregion
    }
}
