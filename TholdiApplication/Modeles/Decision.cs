﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiApplication.Modeles
{
    class Decision
    {
        private short _numContainer;
        private int _numInspection;
        private string _codeTravaux;
        private DateTime _dateEnvoi;
        private DateTime _dateRetour;
        private string _commentaire;

        public short NumContainer
        {
            get
            {
                return _numContainer;
            }
        }

        public Decision()
        {
            _numContainer = -1;
        }

        public int NumInspection
        {
            get
            {
                return _numInspection;
            }
        }

        public string CodeTravaux
        {
            get
            {
                return _codeTravaux;
            }
        }

        public DateTime DateEnvoi
        {
            get
            {
                return _dateEnvoi;
            }

            set
            {
                _dateEnvoi = value;
            }
        }

        public DateTime DateRetour
        {
            get
            {
                return _dateRetour;
            }

            set
            {
                _dateRetour = value;
            }
        }

        public string Commentaire
        {
            get
            {
                return _commentaire;
            }

            set
            {
                _commentaire = value;
            }
        }

        #region Requêtes SQL sur la classe Décision
        private static string _selectSql =
            "SELECT NUMCONTAINER, NUMINSPECTION, CODETRAVAUX, DATEENVOI, DATERETOUR, COMMENTAIRE from DECISION";

        private static string _selectByNumContainerSql =
            "SELECT NUMCONTAINER, NUMINSPECTION, CODETRAVAUX, DATEENVOI, DATERETOUR, COMMENTAIRE from DECISION where NUMCONTAINER = ?numcontainer AND NUMINSPECTION = ?numinspection AND CODETRAVAUX = ?codetravaux";

        private static string _updateSql =
            "UPDATE DECISION SET NUMCONTAINER=?numcontainer , NUMINSPECTION=?numinspection, CODETRAVAUX=?codetravaux, DATEENVOI=?dateenvoi, DATERETOUR=?dateretour, COMMENTAIRE=?commentaire WHERE NUMCONTAINER=?numcontainer AND NUMINSPECTION=?numinspection AND CODETRAVAUX=?codetravaux";

        private static string _insertSql =
            "INSERT INTO DECISION (NUMCONTAINER, NUMINSPECTION, CODETRAVAUX, DATEENVOI, DATERETOUR, COMMENTAIRE) VALUES (?numcontainer,?numinspection,?codetravaux,?dateenvoi, ?dateretour, ?commentaire)";

        private static string _deleteByNumContainerSql =
            "DELETE FROM DECISION WHERE NUMCONTAINER=?numcontainer AND NUMINSPECTION=?numinspection AND CODETRAVAUX=?codetravaux";

        private static string _getLastInsertNumContainer =
            "SELECT NUMCONTAINER FROM DECISION WHERE NUMINSPECTION = ?numinspection AND CODETRAVAUX=?codetravaux AND DATEENVOI=?dateenvoi AND DATERETOUR=?dateretour AND COMMENTAIRE=?commentaire";
        #endregion

        public static Decision Fetch(int numContainer)
        {
            Decision d = null;
            MySqlConnection msc = DataBaseAccess.Connexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByNumContainerSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?NUMCONTAINER", numContainer));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                d = new Decision();//Initialisation de la variable Decision
                d._numContainer = Convert.ToInt16(jeuEnregistrements["NUMCONTAINER"].ToString());//Lecture d'un champ de l'enregistrement
                d._numInspection = Convert.ToInt16(jeuEnregistrements["NUMINSPECTION"].ToString());
                d._codeTravaux = Convert.ToString(jeuEnregistrements["CODETRAVAUX"].ToString());
                d._dateEnvoi = Convert.ToDateTime(jeuEnregistrements["DATEENVOI"].ToString());
                d._dateRetour = Convert.ToDateTime(jeuEnregistrements["DATERETOUR"].ToString());
                d._commentaire = Convert.ToString(jeuEnregistrements["COMMENTAIRE"].ToString());
            }
            msc.Close();//fermeture de la connexion
            return d;
        }

        public static List<Decision> FetchAll()
        {
            List<Decision> resultat = new List<Decision>();
            MySqlConnection msc = DataBaseAccess.Connexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Decision d = new Decision();
                string numContaino = jeuEnregistrements["NUMCONTAINER"].ToString();
                d._numContainer = Convert.ToInt16(numContaino);
                d._numInspection = Convert.ToInt16(jeuEnregistrements["NUMINSPECTION"].ToString());
                d._codeTravaux = Convert.ToString(jeuEnregistrements["CODETRAVAUX"].ToString());
                d._dateEnvoi = Convert.ToDateTime(jeuEnregistrements["DATEENVOI"].ToString());
                d._dateRetour = Convert.ToDateTime(jeuEnregistrements["DATERETOUR"].ToString());
                d._commentaire = Convert.ToString(jeuEnregistrements["COMMENTAIRE"].ToString());
                resultat.Add(d);
            }
            msc.Close();
            return resultat;
        }

    }
}
