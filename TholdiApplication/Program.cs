﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TholdiApplication.Modeles;
using TholdiApplication.Vues;

namespace TholdiApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            
            List<Probleme> collectionProbleme = Probleme.FetchAll();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormPagePrincipal());

        }
    }
}
